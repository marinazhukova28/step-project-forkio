Модуль: advanced-html-css
Проект: step-project-forkio

Используемые технологии:

    -html
    -css
    -scss
    -js
    -gulp
    -Node.js
    -методология BEM
    
Участники проекта:

    Марина Жукова:
        -Сверстала шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана).
        -Сверстала секцию People Are Talking About Fork.
        
    Лилия Баран:
        -Светстала секцию Here is what you get.
        -Сверстала блок Revolutionary Editor.
        -Сверстала секцию Fork Subscription Pricing. 
